/* file:           charts.js
 * version:        1.3
 * last changed:   23.03.2014
 * description:    This file can be removed before you use template in production. 
 *                 It contains with elements used only for charts demo preivew
*/

$(document).ready(function(){
    
    gCharts = {        
        init: function(){

            if( $("#charts-lineplot").length > 0 ){

            // Rickshas charts

                // line chart
                var sin = [], cos = [], sin2 = [];

                    for (var i = 0; i < 10; i += 0.3) {
                        sin.push({x: i, y: Math.sin(i)})            
                        sin2.push({x: i, y: Math.sin(i-1.57)});
                        cos.push({x: i, y: Math.cos(i)});            
                    }

                var rlp = new Rickshaw.Graph( {	
                    element: document.getElementById("charts-lineplot"),
                    renderer: 'lineplot',
                    min: -1.2,
                    max: 1.2,
                    padding: { top: 0.1 },
                    series: [{data: sin,color: '#2f9fe0',name: "sin"}, 
                             {data: sin2,color: '#8CC152',name: "sin2"},
                             {data: cos,color: '#DA4453',name: "cos"}]
                });

                var hover = new Rickshaw.Graph.HoverDetail({ graph: rlp });

                rlp.render();

                var rlp_resize = function() {                
                    rlp.configure({
                            width: $("#charts-lineplot").width(),
                            height: $("#charts-lineplot").height()
                    });
                    rlp.render();
                }

                window.addEventListener('resize', rlp_resize); 
                rlp_resize();
                // eof lineplot


                // Line chart
                var seriesData = [ [], [], [] ];
                var random = new Rickshaw.Fixtures.RandomData(50);

                for (var i = 0; i < 50; i++) {
                        random.addData(seriesData);
                }

                var rlc = new Rickshaw.Graph( {
                        element: document.getElementById("charts-lines"),
                        renderer: 'line',
                        min: 50,
                        series: [{color: "#2f9fe0",data: seriesData[0],name: 'New York'}, 
                                 {color: "#8CC152",data: seriesData[1],name: 'London'}, 
                                 {color: "#DA4453",data: seriesData[2],name: 'Tokyo'}]
                });

                rlc.render();    

                var hoverDetail = new Rickshaw.Graph.HoverDetail({graph: rlc});
                var axes = new Rickshaw.Graph.Axis.Time({graph: rlc});
                axes.render();

                    var rlc_resize = function() {                
                                rlc.configure({
                                        width: $("#charts-lines").width(),
                                        height: $("#charts-lines").height()
                                });
                                rlc.render();
                        }

                    window.addEventListener('resize', rlc_resize); 
                    rlc_resize();
                // eof line chart

           } 
        }
    }
    gCharts.init();
        
});

function gChart(){
    gCharts.init();
}
