
var SESSION_DATA_NAME = "assist-session-config";
var controllerWindow;

var statusDiv = document.getElementById("status");
var screenData = {};
var observer;

function LocalTopic(metadata) {
  this.metadata = metadata;
  this.local = true;

  this.sendMessage = function(payload) {
    console.log("LOCAL MESSAGE");
  }
  this.openSubtopic = function(metadata, callBack) {
    var newTopic = new LocalTopic(metadata)
    callBack(newTopic);
  };
  this.closeTopic = function(metadata) {};
  this.joinTopic = function(metadata) {};            
  this.leaveTopic = function(metadata) {};
}

window.AssistSDK = {
    sourceWindow : null,
    desination : null,
    supportDiv : null,
    video : null,
    divX : 20,
    divY : 20,
    localVideo : null,
    remoteVideo : null,
    nameDiv : null,
    currentCall : null,
    statusDiv : null,
    updateScheduled : false,
    fullWidth : 0,
    fullHeight : 0,
    glassPane : null,
    socketClosed : true,
    jQuery : null,
    loaded : false,
    cleanUpStack : new Array(),
    universalTimeout : null,
    agentPictureUrl : null,
    muted : false,
    renderVideoWindow : true,

    rootTopic : null,
    screenShareWindow : null,
    videoWindowTopic : null,
    videoWindow : null,
    cqClasses : ["no-call", "call-quality-good", "call-quality-moderate", "call-quality-poor"],
    inputTopic : null,
    inputUid : 0,
    inputElements : [],

    screenShareAllowed : false,
    debugMode : false,

    startSupport : function() {      
        var config = window.AssistConfig;
        AssistSDK.sourceWindow = window.opener;

        window.onunload = function() {
            AssistSDK.endSupport();
        };
        
        if (config == false) {
            return;
        }
        
        AssistSDK.debugMode = config.getDebugMode();
        console.log("debugMode = " + AssistSDK.debugMode);
        
        if (config.getSessionToken()) {
            AssistSDK.start(config);
        } else {
            var request = new XMLHttpRequest();

            var url = "/assistserver/consumer";
            var postData = "type=create&targetServer=" + config.getTargetServer()
                         + "&originServer=" + config.getOriginServer();

            if (config.getCorrelationId()) {
                postData = postData + "&username=" + config.getCorrelationId();
            }

            if (config.getUrl()) {
                url = config.getUrl() + url;
            }
            
            console.log("startSupport(): url = " + url);
            
            request.open("POST", url, true);
            request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            request.onreadystatechange = function () {
                if (request.readyState == 4) {
                    if (request.status == 200) {
                        var result = JSON.parse(request.responseText);
                        var correlationId = result.address;
                        var sessionToken = result.token;
                        config.setSessionToken(sessionToken);
                        AssistSDK.start(config); 
                    }
                }
            };

            request.send(postData);
        }
    },

    allowScreenShare : function() {
        AssistSDK.screenShareAllowed = true;
        if (AssistSDK.rootTopic) {
            AssistSDK.startScreenShare();
        }
    },

    start : function(config) {
        if (config.hasDestination()) {
            console.log("Connecting with the following destination: " + config.getDestination());
            AssistSDK.connectWithAudio(config);
        }
        else {
            console.log("Connecting with following correlation ID: " + config.getCorrelationId());
            AssistSDK.connectWithoutAudio(config);
        }
    },

    log : function(message) {
        console.log(message);   
    },

    reconnect : function(source) {
    	AssistSDK.isOnRecognizedAssistPage = true;
        AssistSDK.sourceWindow = source;
        AssistSDK.createRemoteUI((AssistConfig.hasVideo() == false)); //expects "true" if no video
        
        if (!AssistSDK.isOnAssistPages) {
    		AssistSDK.isOnAssistPages = true;
    		AssistSDK.sendOnAssistPagesMessage();
    	}
        
        AssistSDK.updateScreen(true);
    },

    endSupport : function() {
        AssistSDK.log("Unloading assist window");
        AssistConfig.unset();
        
        if (AssistSDK.glassPane.parentNode != undefined) {
            AssistSDK.glassPane.parentNode.removeChild(AssistSDK.glassPane);
        }

        if (AssistSDK.currentCall != undefined) {
            if (AssistSDK.currentCall != null) {
                AssistSDK.currentCall.end();
                AssistCSDK.destroySession();
                AssistSDK.currentCall = null;
            }
        }

        for (var index in AssistSDK.cleanUpStack) {
            try {
                AssistSDK.cleanUpStack[index]();
            } catch(e) {
                console.log(e);
            }
        }
        AssistSDK.cleanUpStack = new Array();
        
        if (AssistSDK.sourceWindow && AssistSDK.sourceWindow.AssistSDK.onEndSupport) {
            try {
                if (!("endSupportCalled" in AssistSDK && AssistSDK.endSupportCalled == true)) {
                    AssistSDK.sourceWindow.AssistSDK.supportEnded();
                }
                AssistSDK.endSupportCalled = true;
            } 
            catch(e) {}
        }

        if (!AssistSDK.debugMode) {
        	window.close();
        }
    },

    displayModal : function(message) {
        var document = AssistSDK.sourceWindow.document;
        var modalContainer = document.createElement("div");
        modalContainer.id = "assist-support-rejected-modal";
        document.body.appendChild(modalContainer);

        var modal = document.createElement("div");

        modalContainer.appendChild(modal);

        var p = document.createElement("p");
        p.innerHTML = message;
        modal.appendChild(p);

        var input = document.createElement("input");
        input.type = "button";
        input.value = i18n.t("assistI18n:button.ok");
        p.appendChild(input);

        input.addEventListener("click", function(event) {
            var doc = event.target.ownerDocument;
            var modal = doc.getElementById("assist-support-rejected-modal");
            modal.parentNode.removeChild(modal);
            
        }, false);
    },

    displaySupportBusyModal : function() {
    	 AssistSDK.displayModal(i18n.t("assistI18n:notice.noAgents"));
    },
    
    setAgentName : function(agentName) {
        AssistSDK.log("Setting agentName to " + agentName);
        if (!agentName || agentName == 'undefined') {
            return;
        }

        AssistSDK.agentName = agentName;
        if (AssistSDK.nameDiv != null) {
            AssistSDK.nameDiv.firstChild.nodeValue = agentName;
        }
    },

    setAgentText : function(agentText) {
        AssistSDK.log("Setting agentText (tagline) to " + agentText);
        if (!agentText || agentText == 'undefined') {
            return;
        }
        
        AssistSDK.agentText = agentText;
        if (AssistSDK.taglineDiv != null) {
            AssistSDK.taglineDiv.firstChild.nodeValue = agentText;
        }
    },

    setAgentPicture : function(agentPictureUrl) {
        AssistSDK.log("Setting agentPictureUrl to " + agentPictureUrl);
        if (!agentPictureUrl || agentPictureUrl == 'undefined') {
            return;
        }

        AssistSDK.agentPictureUrl = agentPictureUrl;
        if ((AssistSDK.picture !== null) && (typeof AssistSDK.picture !== "undefined")) {
            AssistSDK.picture.setAttribute("src", agentPictureUrl);
        }
    },

    drawAnnotation : function() {
    },

    getAnnotationWindow : function() {
    	for (var i = 0; i < AssistSDK.screenShareWindow.children.length; i++) {
    		var child = AssistSDK.screenShareWindow.children[i];
    		if (child instanceof AnnotationWindow)
    			return child;
    	}
    	return null;
    },
    
    getSpotlightWindow : function() {
        for (var i = 0; i < AssistSDK.screenShareWindow.children.length; i++) {
    		var child = AssistSDK.screenShareWindow.children[i];
    		if (child instanceof SpotlightWindow)
    			return child;
    	}
    	return null;
    },

    clearAnnotations : function() {
        var annotationWindow = AssistSDK.getAnnotationWindow();
        if (annotationWindow != null) {
        	annotationWindow.clear(true);
        }
        
        // Spotlightwindow does not have a 'clear' method.
        
        // var spotlightWindow = AssistSDK.getSpotlightWindow();
        // if (spotlightWindow != null) {
        //     spotlightWindow.clear(true);
        // }
    },

    capture : function(callback) {
        AssistSDK.log("Capture screen and send");
        
        try {
            window.html2canvas(window.opener.document.body, callback);
        } catch(e) {
            console.log(e);
        }

    },

    updateScreen : function(force) {
        if (window == null) {
            //The connection window has already been closed; can't send the screen
            return;
        }
        var updateScheduled = AssistSDK.updateScheduled;
        if (updateScheduled == true) {
            AssistSDK.log("update already scheduled, not rendering");
            return;
        }

        if (observer != undefined) {
            if (observer != null) {
                try {
                    observer.disconnect();
                } catch(e) {}
            }
        }

        AssistSDK.fullWidth = AssistSDK.sourceWindow.innerWidth;
        AssistSDK.fullHeight = AssistSDK.sourceWindow.innerHeight;

        AssistSDK.capture({
            onrendered : function(canvas) {
                
                if (AssistSDK.screenShareWindow) {
                    try {  // strip off scrollbar space
                        var clientWidth = AssistSDK.sourceWindow.document.documentElement.clientWidth;
                        var clientHeight = AssistSDK.sourceWindow.document.documentElement.clientHeight;
                        
                        if (canvas.width > clientWidth || canvas.height > clientHeight) {
                            
                            if (isIE) {
                                var img = new Image();
                                img.src = canvas.toDataURL("image/png");
                                canvas = img;
                            }
                            var croppedCanvas = document.createElement("canvas");
                            croppedCanvas.width = clientWidth;
                            croppedCanvas.height = clientHeight;
                            var context = croppedCanvas.getContext("2d");
                            context.drawImage(canvas, 0, 0);
                            canvas = croppedCanvas;
                        }
                    } catch(e) {
                        console.log(e);
                    }
                    
                    AssistSDK.scheduleObserver();
                    AssistSDK.screenShareWindow.contentChanged(canvas, force);
                }
            },
            width: AssistSDK.fullWidth,
            height: AssistSDK.fullHeight,
            useCORS: true
        });
    },

    scheduleObserver : function() {
        // configuration of the observer:
        
        var config = {
            attributes : true,
            childList : true,
            characterData : true,
            subtree : true
        };

        var isChildOf = function(candidateChild, candidateParent) {
            var parentNode = candidateChild.parentNode;
            if (parentNode) {
                if (parentNode == candidateParent) {
                    return true;
                } else {
                    return isChildOf(parentNode, candidateParent);
                }
            } else {
                return false;
            }
        };

        if (observer == null || typeof observer === 'undefined') {
            AssistSDK.log("init observer");
            observer = new MutationObserver(function(mutations) {
                var rerender = true;
                for (var i = 0; i < mutations.length; i++) {
                    var mutation = mutations[i];
                    if (mutation.target == AssistSDK.supportDiv) {
                        rerender = false;
                        observer.takeRecords();
                        break;
                    }
                    if (mutation.target == AssistSDK.glassPane || isChildOf(mutation.target, AssistSDK.glassPane)) {
                        rerender = false;
                        observer.takeRecords();
                        break;
                    }
                }

                if (rerender == true) {
                    clearTimeout(AssistSDK.universalTimeout);
                    AssistSDK.log("do rerender (mutations)");
                    AssistSDK.universalTimeout = setTimeout(AssistSDK.updateScreen, 500);
                }

            });
        }

        console.log("scheduling observer");
        // pass in the target node, as well as the observer options
        try {
            observer.observe(AssistSDK.sourceWindow.document.body, config);
        } catch(e) {
            console.warn(e);
        }
    },

    registerScrollListeners : function() {
        var eventTarget;
        var $ = AssistSDK.jQuery;
        var document = AssistSDK.sourceWindow.document;
        var window = AssistSDK.sourceWindow;
        var scroll = "scroll";
        var ns = ".assist";
        var scrollX = -1;
        var scrollY = -1;

        function doRender(event) {
            AssistSDK.log("do rerender (listeners)");
            clearTimeout(AssistSDK.universalTimeout);
            AssistSDK.universalTimeout = setTimeout(AssistSDK.updateScreen, 750);
        }

        function documentScrollCallback(event) {
        	console.log("In documentScrollCallback()");
            if (event.target == document || event.target == document.body) {
                var newScrollX = window.pageXOffset;
                var newScrollY = window.pageYOffset;

                // this is all a hack to figure out whether the scroll is legitimate or triggered
                // by html2canvas, in which case, the sum scroll will be 0 (without this we loop)
                if (scrollX != newScrollX || scrollY != newScrollY) {
                    scrollX = newScrollX;
                    scrollY = newScrollY;
                    doRender(event);
                }
                
                console.log("Clearing annotations...");
                AssistSDK.clearAnnotations();
            }
        }

        function mousewheelScroll(event) {
            // the $(document).scroll(doRender); line will catch these so don't bother
            if (event.target !== document && event.target !== document.body) {
                doRender(event);
            }
        }

        function keyDown(event) {
            switch (event.which) {
                case 33: // page up
                case 34: // page down
                case 37: // left
                case 38: // up
                case 39: // right
                case 40: // down
                    doRender(event);
                    return;
                default: return;
            }
        }

        function mouseDown(event) {
            eventTarget = event.target;
            if (event.target !== document && event.target !== document.body) {
                $(eventTarget).on(scroll + ns, function(event) {
                    //$(eventTarget).off(ns);
                    doRender(event);
                });
            }
        }

        function mouseUp(event) {
            $(eventTarget).off(ns);
        }

        $(document).scroll(documentScrollCallback);
        $(document.body).on("mousewheel DOMMouseScroll", mousewheelScroll);
        $(document.body).keydown(keyDown);
        $(document.body).mousedown(mouseDown);
        $(document.body).mouseup(mouseUp);

        (function prepareCleanUp() {
            function doCleanUp() {
                $(document).off("scroll", documentScrollCallback);
                $(document.body).off("mousewheel DOMMouseScroll", mousewheelScroll);
                $(document.body).off("keydown", keyDown);
                $(document.body).off("mousedown", mouseDown);
                $(document.body).off("mouseup", mouseUp);
                $(eventTarget).off(ns);
                clearTimeout(AssistSDK.universalTimeout);
            }

            AssistSDK.cleanUpStack.push(doCleanUp);
        })();

    },

    setupVideoWindowObserver : function() {
        var videoWindow = AssistSDK.supportDiv;
        var $ = AssistSDK.jQuery;

        var config = {
            attributes : true,
            childList : true,
            characterData : true,
            subtree : true
        };

        var timeout;
        var windowObserver = new MutationObserver(function(mutations) {
            if (AssistSDK.renderVideoWindow !== false) {
                clearTimeout(timeout);
                timeout = setTimeout(AssistSDK.updateVideoWindow, 500);
            } else {
                AssistSDK.renderVideoWindow = true;
            }
        });
        windowObserver.observe(videoWindow, config);

        AssistSDK.updateVideoWindow = function(force) {
            if (AssistSDK.sourceWindow.getComputedStyle(videoWindow).display == "none") {
                // TODO Don't display, either create a new message type or send a resize with 0 x and y
            } else {
                try {
                    windowObserver.disconnect();
                    observer.disconnect();
                } catch(e) {}
                html2canvas(videoWindow, {
                    overrideIgnore: true,
                    onrendered: function(canvas) {
                        if (AssistSDK.videoWindow) {
                            AssistSDK.scheduleObserver();
                            AssistSDK.videoWindow.contentChanged(canvas, force);
                        }
                    },
                    useCORS: true
                });
            }
        };
    
        (function prepareCleanUp() {
            function doCleanUp() {
                clearTimeout(timeout);
                try {
                    windowObserver.disconnect();
                    observer.disconnect();
                } catch(e) {}
            }

            AssistSDK.cleanUpStack.push(doCleanUp);
        })();
        
        AssistSDK.updateVideoWindow();
    },

    mapInputElements: function(document) {
        // TODO find all the input elements in the passed document and figure out the most appropriate label for each of them
        // We could use getElementsByTagName("*") here and then filter but the resultant list would be live so the index of an element could change
        // Which won't work for us, hence this awkward solution
        var findInputs = function(element) {
            var inputs = [];
            if (element.childNodes && element.childNodes.length > 0) {
                for (var i =  0; i < element.childNodes.length; i++) {
                    inputs = inputs.concat(findInputs(element.childNodes[i]));
                }
            }
            if (element.tagName == "INPUT" || element.tagName == "SELECT" || element.tagName == "TEXTAREA") {
                // Do not show elements that have been masked.
                if (!element.classList.contains("assist-no-show")) {
                    inputs[inputs.length] = element;
                }
            }
            // Sort the inputs by tab-index
            // TODO this sort function may not be appropriate as it is not guaranteed to be stable and we are relying on its stability
            inputs.sort(function(a, b) {
                var val = a - b;
                if (val < 0 && a < 0) {
                    return 1; // a is -1, b is not so b is first
                } else if (val > 0 && b < 0) {
                    return -1; // b is -1, a is not so a is first
                }
                return val;
            });
            return inputs;
        };
        var inputs = findInputs(document);
        var labels = document.getElementsByTagName("label");

        var inputDescriptors = [];
        // TODO this could be more efficient but should be pretty fast anyway
        for (var i = 0; i < inputs.length; i++) {
            var input = inputs[i];
            var label = undefined;
            var type;
            if (input.tagName == "INPUT") {
                type = input.getAttribute("type");
            } else {
                type = input.tagName.toLowerCase();
            }
            if (input.id) {
                for (var j = 0; j < labels.length; j++) {
                    if (labels[j].getAttribute("for") == input.id) {
                        label = labels[j].innerText;
                        break;
                    }
                }
            }
            // TODO we may need to define a special attribute which can be used to label inputs which otherwise end up with a rubbish label
            if (!label) {
                label = input.getAttribute("title");
            }
            if (!label && type == "radio") {
                label = input.getAttribute("value");
            }
            if (!label) {
                label = input.getAttribute("name");
            }
            if (!label) {
                label = input.id;
            }
            // There is no point sending a descriptor to the agent if we have absolutely no label so only continue if we have some sort of descriptor
            // Except if it is a list box which might be sufficiently self explanatory
            if (label || type == "select") {
                var elementInputDescriptor = {type: type, label: label, index: i};
                // TODO Add type specific initial value attributes
                switch (type) {
                    case "radio":
                        elementInputDescriptor.radioGroup = input.getAttribute("name");
                        // fall through
                    case "checkbox":
                        elementInputDescriptor.checked = input.getAttribute("checked");
                        break;
                    case "text":
                    case "textarea":
                        var pattern = input.getAttribute("pattern");
                        if (pattern) {
                            elementInputDescriptor.pattern = pattern;
                        }
                        // fall through
                    case "email":
                        var value = input.getAttribute("value");
                        if (value) {
                            elementInputDescriptor.value = value;
                        }
                        var placeholder = input.getAttribute("placeholder");
                        if (placeholder) {
                            elementInputDescriptor.placeholder = placeholder;
                        }
                        break;
                    case "password":
                    case "submit":
                        continue;
                    case "select":
                        var options = input.getElementsByTagName("option");
                        var optionDescs = [];
                        for (var j = 0; j < options.length; j++) {
                            optionDescs[optionDescs.length] = {value: options[j].value, label: options[j].textContent};
                        }
                        elementInputDescriptor.options = optionDescs;
                        break;
                    }
                inputDescriptors[inputDescriptors.length] = elementInputDescriptor;
                input.addEventListener("change", function() {
                    var inputElement = input;
                    var descriptor = elementInputDescriptor;
                    var inputType = type;
                    return function () {
                        switch (inputType) {
                            case "text":
                            case "textarea":
                            case "select":
                                descriptor.value = inputElement.value;
                                break;
                            case "radio":
                            case "checkbox":
                                descriptor.checked = inputElement.checked;
                                break;
                        }
                        // Send an updated descriptor to the agent
                        var updateDescriptor = {screenId: AssistSDK.inputDescriptor.screenId, descriptors: [descriptor]};
                        var descriptorString = JSON.stringify(updateDescriptor);
                        var message = new Uint8Array(descriptorString.length + 2);
                        var header = new Int16Array(message.buffer, 0, 2);
                        header[0] = INPUT_ELEMENTS_POPULATED;
                        var payload = new Uint8Array(message.buffer, 2);
                        for (var i = 0; i < descriptorString.length; i++) {
                            payload[i] = descriptorString.charCodeAt(i);
                        }
                        AssistSDK.inputTopic.sendMessage(message);
                        AssistSDK.updateScreen();
                    }
                }());
                if (type == "text" || type == "textarea" || type == "email") {
                    input.addEventListener("click", function () {
                        var elementDescriptor = elementInputDescriptor;
                        return function (event) {
                            // If the click event was sent from the remote endpoint then notify the
                            if (event.assist_generated && AssistSDK.inputTopic && AssistSDK.inputDescriptor) {
                                // update placeholder
                                var inputElement = AssistSDK.inputElements[elementDescriptor.index];
                                var placeholder = inputElement.getAttribute("placeholder") || inputElement.value;
                                if (placeholder) {
                                    elementDescriptor.placeholder = placeholder;
                                }
                                var desc = {screenId: AssistSDK.inputDescriptor.screenId, clicked: elementDescriptor};
                                var descString = JSON.stringify(desc);
                                var message = new Uint8Array(descString.length + 6);
                                var header = new Int16Array(message.buffer, 0, 3);
                                header[0] = INPUT_ELEMENT_CLICKED;
                                // These values will probably be slightly off, but being able to see the remote text box behind
                                // is generally better anyway
                                var bounds = event.target.getBoundingClientRect();
                                header[1] = bounds.left;
                                header[2] = bounds.top;
                                var payload = new Uint8Array(message.buffer, 6);
                                for (var i = 0; i < descString.length; i++) {
                                    payload[i] = descString.charCodeAt(i);
                                }
                                AssistSDK.inputTopic.sendMessage(message);
                            }
                        };
                    }());
                }
            }
        }
        if (inputDescriptors.length > 0) {
            AssistSDK.inputElements = inputs;
            // We need a unique ID to prevent data for one form being used to populate another
            var pageId = ++AssistSDK.inputUid;
            AssistSDK.inputDescriptor = {screenId: pageId, descriptors: inputDescriptors};
            // Create the form info topic under the screen share topic if it doesn't already exist
            var sendInputDescriptor = function () {
                var jsonPayload = JSON.stringify(AssistSDK.inputDescriptor);
                var message = new Uint8Array(jsonPayload.length + 2);
                var header = new Int16Array(message.buffer, 0, 1);
                var payload = new Uint8Array(message.buffer, 2);
                header[0] = INPUT_ELEMENTS_ON_PAGE;
                for (var i = 0; i < jsonPayload.length; i++) {
                    payload[i] = jsonPayload.charCodeAt(i);
                }
                AssistSDK.inputTopic.sendMessage(message);
            };
            if (AssistSDK.inputTopic && AssistUtils.hasAgent(AssistSDK.inputTopic)) {
                sendInputDescriptor();
            } else {
                var metadata = {type: "input"};
                // We're not a member of an input topic so create a new one
                AssistSDK.rootTopic.openSubtopic(metadata, function (subtopic) {
                    AssistSDK.inputTopic = subtopic;
                    subtopic.participantJoined = function (newParticipant) {
                        if (newParticipant.metadata.role == "agent") {
                            sendInputDescriptor();
                        }
                    };
                    subtopic.messageReceived = function(source, message) {
                        var type = new Int16Array(message.buffer, 0, 1)[0];

                        switch (type) {
                            case INPUT_ELEMENTS_POPULATED:
                                var messagePayload = new Uint8Array(message.buffer, 2);
                                var populatedElementsString = String.fromCharCode.apply(null, messagePayload);
                                var populatedElements = JSON.parse(populatedElementsString);
                                if (populatedElements.screenId != AssistSDK.inputUid) {
                                    // The populated element description refers to a form on another page, ignore it
                                    return;
                                }
                                var alteredElements = populatedElements.descriptors;
                                for (var i = 0; i < alteredElements.length; i++) {
                                    var altered = alteredElements[i];
                                    var element = AssistSDK.inputElements[altered.index];
                                    var type;
                                    if (element.tagName == "INPUT") {
                                        type = element.getAttribute("type");
                                    } else {
                                        type = element.tagName.toLowerCase();
                                    }
                                    switch (type) {
                                        case "text":
                                        case "select":
                                        case "textarea":
                                        case "email":
                                            element.value = altered.value;
                                            break;
                                        case "radio":
                                        case "checkbox":
                                            element.checked = altered.checked;
                                            break;
                                    }
                                    if (altered.clickNext) {
                                        // In order to try to simulate tabbing through the document naturally, find the next element in the tab order
                                        // and if it is a form element, simulate an agent click on it.
                                        // This is not really possible but grabbing the next element in the descriptor list should be a good proxy
                                        for (var i = 0; i < AssistSDK.inputDescriptor.descriptors.length; i++) {
                                            if (altered.index == AssistSDK.inputDescriptor.descriptors[i].index) {
                                                for(var nextDescriptorIndex = i + 1; nextDescriptorIndex < AssistSDK.inputDescriptor.descriptors.length; nextDescriptorIndex++ ) {
                                                    var nextDescriptor = AssistSDK.inputDescriptor.descriptors[nextDescriptorIndex];
                                                    if (nextDescriptor) {
                                                        var nextElement = AssistSDK.inputElements[nextDescriptor.index];
                                                        if (nextElement.tagName == "TEXTAREA" ||
                                                            (nextElement.tagName == "INPUT" && (nextElement.getAttribute("type") == "text") || nextElement.getAttribute("type") == "email")) {
                                                            var bounds = nextElement.getBoundingClientRect();
                                                            var viewWidth = AssistSDK.sourceWindow.innerWidth;// || document.body.clientWidth;
                                                            var viewHeight = AssistSDK.sourceWindow.innerHeight;// || document.body.clientHeight;
                                                            var scrollX = (window.pageXOffset === undefined ? AssistSDK.sourceWindow.document.body.scrollLeft : AssistSDK.sourceWindow.pageXOffset);
                                                            var scrollY = (window.pageYOffset === undefined ? AssistSDK.sourceWindow.document.body.scrollTop : AssistSDK.sourceWindow.pageYOffset);
                                                            if (bounds.top > scrollY && bounds.bottom < (scrollY + viewHeight)
                                                                && bounds.left > scrollX && bounds.bottom < (scrollX + viewWidth)) {
                                                                // Only send the click if the element is entirely visible
                                                                var desc = {screenId: AssistSDK.inputDescriptor.screenId, clicked: nextDescriptor};
                                                                var descString = JSON.stringify(desc);
                                                                var message = new Uint8Array(descString.length + 6);
                                                                var header = new Int16Array(message.buffer, 0, 3);
                                                                header[0] = INPUT_ELEMENT_CLICKED;
                                                                // These values will probably be slightly off, but being able to see the remote text box behind
                                                                // is generally better anyway
                                                                header[1] = bounds.left;
                                                                header[2] = bounds.top;
                                                                var payload = new Uint8Array(message.buffer, 6);
                                                                for (var i = 0; i < descString.length; i++) {
                                                                    payload[i] = descString.charCodeAt(i);
                                                                }
                                                                AssistSDK.inputTopic.sendMessage(message);
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                        clearTimeout(AssistSDK.universalTimeout);
                        AssistSDK.log("Scheduling a screen update (Form filled)");
                        AssistSDK.universalTimeout = setTimeout(AssistSDK.updateScreen, 500);
                    };
                });
            }
        } else {
            // There are no inputs to send so clear the descriptors and leave the input topic if there is one
            if (AssistSDK.inputTopic) {
                AssistSDK.inputTopic.leave();
                AssistSDK.inputTopic = undefined;

            }
            AssistSDK.inputDescriptor = {};
        }
    },

    createRemoteUI : function(skipVideo) {
        screenData = {};
        AssistSDK.log("TOPICSOCKET: " + AssistSDK.topicSocket);

        if (AssistSDK.rootTopic) {
            // If we already have a root topic connected then find and transmit any forms
            AssistSDK.mapInputElements(AssistSDK.sourceWindow.document.body);
        }
        jQueryLoaded($);

        (function loadCSS() {
            if (AssistSDK.sourceWindow.document.getElementById("ASSIST-CSS") != null) {
                return;
            }

            AssistUtils.loadCSS(AssistSDK.sourceWindow.document, AssistUtils.getSdkPath() + "css/assist.css", "ASSIST-CSS");
            AssistUtils.loadCSS(AssistSDK.sourceWindow.document, AssistUtils.getSdkPath() + "../shared/css/shared-window.css");
            if (typeof i18n !== "undefined" && i18n !== null && AssistUtils.isRTL(i18n.lng())) {
                AssistUtils.loadCSS(AssistSDK.sourceWindow.document, AssistUtils.getSdkPath() + "css/rtl.css");
                AssistUtils.loadCSS(AssistSDK.sourceWindow.document, AssistUtils.getSdkPath() + "../shared/css/shared-window-rtl.css");
            }
        })();

        (function addPageUnloadListeners() {
            if ("onpagehide" in window) {
                AssistSDK.sourceWindow.addEventListener("pagehide",
                    AssistSDK.handleUnload, false);
            } else {
                AssistSDK.sourceWindow.addEventListener("unload",
                    AssistSDK.handleUnload, false);
            }

            (function prepareCleanUp() {
                function doCleanUp() {
                    if ("onpagehide" in window) {
                        AssistSDK.sourceWindow.removeEventListener("pagehide", AssistSDK.handleUnload, false);
                    } else {
                        AssistSDK.sourceWindow.removeEventListener("unload", AssistSDK.handleUnload, false);
                    }
                }

                AssistSDK.cleanUpStack.push(doCleanUp);
            })();
        })();

        // don't proceed until jQuery has loaded
        function jQueryLoaded(jQuery) {
            console.log("jQuery loaded callback");

            AssistSDK.jQuery = jQuery;

            if (AssistSDK.glassPane && !isIE) {
                // TODO handle failure of the adoptNode call

                // We need to re-create the support DIV (or probably just the video element, but we already have the code to replace the whole thing)
                // because a video element which has been on another document will not continue to play (it'll just freeze on the last frame it played
                // or if the src is re-set it will render a single frame from the source)
                if (window.chrome) {
                    var oldSupportDiv = AssistSDK.supportDiv;
                    AssistSDK.glassPane.removeChild(oldSupportDiv);
                }
                
                AssistSDK.glassPane = AssistSDK.sourceWindow.document.adoptNode(AssistSDK.glassPane);
                
                if (window.chrome) {
                    // create a new Support DIV
                    AssistSDK.createSupportDiv((skipVideo == false));
                    if (AssistSDK.videoWindow) {
                        AssistSDK.videoWindow.elementChanged(oldSupportDiv, AssistSDK.supportDiv);
                    }
                }
                
                AssistSDK.updateScreen();
            } else {
                AssistSDK.glassPane = AssistSDK.sourceWindow.document
                    .createElement("div");
                AssistSDK.glassPane.id = "glass-pane";
                AssistSDK.glassPane.setAttribute("data-html2canvas-ignore", "true");
                console.log("Creating glass pane");
                
                AssistSDK.createSupportDiv((skipVideo == false));
  
                AssistSDK.glassPane.appendChild(AssistSDK.supportDiv);
                //If in mid-call (reconnected), redraw connection quality indicator.
                if (AssistSDK.currentCall != null) {
                    AssistSDK.currentCall.onConnectionQualityChanged(AssistSDK.connectionQuality);
                }
            }

            AssistSDK.sourceWindow.document.body.appendChild(AssistSDK.glassPane);
            AssistSDK.sourceWindow.focus();
            observer = null; //ie needs this to not try and reconnect observer created from previous DOM
            AssistSDK.scheduleObserver();

            if (AssistSDK.screenShareWindow) {
                AssistSDK.glassPane.appendChild(AssistSDK.supportDiv);
                
                if (isIE) {
                    console.log("ie reconnecting topics");
                    AssistSDK.screenShareWindow.element = AssistSDK.glassPane;
                    AssistSDK.screenShareWindow.children = [];
                        
                    AssistSDK.screenShareWindow.topic.subtopics.forEach(function(subtopic) {
                        
                        console.log("reconnecting topic " + subtopic.metadata.type);
                        try {
                            console.log("leaving topic " + subtopic.metadata.type);
                            subtopic.leave();
                        } catch(e) {
                            // don't really care
                            console.log(e);
                        }
                        
                        AssistSDK.screenShareWindow.topic.subtopicOpened(subtopic);
                    });
                    /*// close all subtopics because IE can't handle moving DOM elements between documents
                    AssistSDK.screenShareWindow.children.forEach(function(child) {
                        child.close();
                    });
                    AssistSDK.screenShareWindow.children = [];*/
                } else {
                    AssistSDK.screenShareWindow.ownerDocumentChanged();
                }
            } else {

              var localTopic = new LocalTopic({})
              
              AssistSDK.screenShareWindow = new HostSharedWindow(localTopic, AssistSDK.glassPane);
              AssistSDK.screenShareTopic = localTopic;
              AssistSDK.screenShareWindow.shareSubWindow(AssistSDK.supportDiv, {moveable: true, name:"draggable-agent-window", mustRemainVisiblePixels: 90, mustRemainVisibleBottomPixels: 120}, 
                function(newWindow, newSubtopic) {
                  AssistSDK.videoWindow = newWindow;
                  newSubtopic.participantJoined = function(newVideoParticipant) {
                    setTimeout(function () {
                      AssistSDK.updateVideoWindow(true);
                      AssistSDK.videoWindow.sendSizeAndPosition();
                    }, 500);
                  };
                }
              );
              
            }            

            if (AssistSDK.video.childNodes[0]) {
                AssistSDK.video.childNodes[0].play();
            }
            AssistSDK.registerScrollListeners();
            
            function resizeUpdate() {
                clearTimeout(AssistSDK.universalTimeout);
                AssistSDK.log("do rerender (resize)");
                AssistSDK.universalTimeout = setTimeout(AssistSDK.updateScreen, 500);
            }

            // TODO: this might trigger two canvas renders when we only really want one (plus the window render)
            AssistSDK.sourceWindow.addEventListener("resize", resizeUpdate, false);

            ;(function prepareCleanUp() {
                function doCleanUp() {
                    AssistSDK.sourceWindow.removeEventListener("resize", resizeUpdate, false);
                }

                AssistSDK.cleanUpStack.push(doCleanUp);
            })();
        }
    },

    createSupportDiv : function(withVideo) {
        
        console.log("with video: " + withVideo);
        var className;
        if (!withVideo) {
            className = "without-video";
            if (AssistSDK.usingAudio) {
                className = className + " audio-only";
            } else {
            	className = className + " without-audio";
            }
        } else {
            className = "with-video";
        }

        AssistSDK.supportDiv = AssistSDK.sourceWindow.document
            .createElement("div");
        AssistSDK.supportDiv.id = "assist-sdk";
        AssistSDK.supportDiv.setAttribute("data-html2canvas-ignore", "true");
        AssistSDK.supportDiv.className = className;
        AssistSDK.supportDiv.style.top = AssistSDK.divY + "px";
        AssistSDK.supportDiv.style.left = AssistSDK.divX + "px";

        AssistSDK.video = AssistSDK.sourceWindow.document.createElement("div");
        AssistSDK.video.id = "video";
        AssistSDK.supportDiv.appendChild(AssistSDK.video);

        $(AssistSDK.sourceWindow).on("resize", AssistSDK.handleWindowResize);
        (function prepareCleanUp() {
            function doCleanUp() {
                $(AssistSDK.sourceWindow).off("resize", AssistSDK.handleWindowResize);
            }
            AssistSDK.cleanUpStack.push(doCleanUp);
        })();

        AssistSDK.nameDiv = AssistSDK.sourceWindow.document.createElement("div");
        AssistSDK.nameDiv.id = "name-div";
        var agentName = AssistSDK.agentName;
        AssistSDK.log("agentName is " + agentName);
        if (agentName == null)
            agentName = "";
        var agentNameNode = AssistSDK.sourceWindow.document.createTextNode(agentName);
        AssistSDK.nameDiv.appendChild(agentNameNode);
        AssistSDK.supportDiv.appendChild(AssistSDK.nameDiv);
        
        if (withVideo) {
        	var menuButton = AssistSDK.sourceWindow.document.createElement("div");
        	menuButton.id = "menu-button";
        	menuButton.classList.add("button");
        	menuButton.onclick = AssistSDK.toggleMenu;
        	AssistSDK.supportDiv.appendChild(menuButton);
        	AssistSDK.menuButton = menuButton;
        	var menuDiv = AssistSDK.sourceWindow.document.createElement("div");
        	menuDiv.id = "menu";
        	menuDiv.style.visibility = "hidden";
        	AssistSDK.supportDiv.appendChild(menuDiv);
        	AssistSDK.menuDiv = menuDiv;
            var mute = AssistSDK.sourceWindow.document.createElement("div");
            mute.id = "mute-button";
            mute.classList.add("unmuted");
            mute.classList.add("button");
            mute.onclick = AssistSDK.toggleMute;
            AssistSDK.menuDiv.appendChild(mute);
            AssistSDK.muteButton = mute;
            var br = AssistSDK.sourceWindow.document.createElement("br");
            AssistSDK.menuDiv.appendChild(br);
            var videoToggleButton = AssistSDK.sourceWindow.document.createElement("div");
            videoToggleButton.id = "video-toggle-button";
            videoToggleButton.classList.add(AssistSDK.localVideoEnabled ? "unmuted" : "muted");
            videoToggleButton.classList.add(AssistSDK.videoToggleEnabled ? "enabled" : "disabled");
            videoToggleButton.classList.add("button");
            videoToggleButton.onclick = AssistSDK.toggleVideo;
            AssistSDK.menuDiv.appendChild(videoToggleButton);
            AssistSDK.videoToggleButton = videoToggleButton;
            var br = AssistSDK.sourceWindow.document.createElement("br");
            AssistSDK.menuDiv.appendChild(br);
        } else if (AssistSDK.usingAudio) {
            var mute = AssistSDK.sourceWindow.document.createElement("div");
            mute.id = "mute-button";
            //mute.textContent = i18n.t("assistI18n:button.mute");
            mute.classList.add("unmuted");
            mute.classList.add("button");
            mute.onclick = AssistSDK.toggleMute;
            AssistSDK.supportDiv.appendChild(mute);
            AssistSDK.muteButton = mute;
        }
        
        if (!withVideo) {
            AssistSDK.picture = AssistSDK.sourceWindow.document.createElement("img");
            AssistSDK.picture.id = "picture";
            var agentPictureUrl = AssistSDK.agentPictureUrl;
            AssistSDK.log("agentPictureUrl is " + agentPictureUrl);
            if (agentPictureUrl != null) {
                    AssistSDK.picture.setAttribute("src", agentPictureUrl);
            }
                           
            AssistSDK.supportDiv.appendChild(AssistSDK.picture);
        }
        
        var end = AssistSDK.sourceWindow.document.createElement("div");
        end.id = "end-button";
        //end.textContent = i18n.t("assistI18n:button.end");
        end.onclick = AssistSDK.endSupport;
        end.classList.add("button");
        AssistSDK.supportDiv.appendChild(end);
        AssistSDK.endButton = end;
    
        AssistSDK.statusDiv = AssistSDK.sourceWindow.document
            .createElement("div");
        AssistSDK.statusDiv.id = "status-div";
        AssistSDK.statusDiv.innerHTML = (!AssistSDK.currentCall) ? "<center>" + i18n.t("assistI18n:popup.status.connecting") + "</center>" : "";
        AssistSDK.supportDiv.appendChild(AssistSDK.statusDiv);
        AssistSDK.statusDiv.style.visibility = "visible";
        
        if (withVideo) {        
            AssistSDK.video.style.visibility = "hidden";
            
            if (AssistSDK.currentCall != null) {
                var URL = AssistSDK.sourceWindow.webkitURL || AssistSDK.sourceWindow.URL;
                var video = AssistSDK.sourceWindow.document.createElement("video");
                video.setAttribute("autoplay", "autoplay");
                video.style.width = "100%";
                video.src = URL.createObjectURL(AssistSDK.remoteVideo);
                AssistSDK.video.appendChild(video);
                video.play();
                //AssistSDK.currentCall.setVideoElement(AssistSDK.video); //todo we should be using this instead, but it's noticeably slower
                
                AssistSDK.video.style.visibility = "visible";
                AssistSDK.statusDiv.style.visibility = "hidden";
            }
        }
    },
    
    toggleMenu : function() {
    	AssistSDK.menuDiv.style.visibility = 
    		(AssistSDK.menuDiv.style.visibility == "hidden") ? "visible" : "hidden";
    },

    toggleMute : function() {
        if (AssistSDK.muted == false) {
            AssistSDK.muted = true;
            //AssistSDK.muteButton.textContent = i18n.t("assistI18n:button.unmute");
            AssistSDK.muteButton.classList.remove("unmuted");
            AssistSDK.muteButton.classList.add("muted");
            if (AssistSDK.videoToggleButton != null) {
                AssistSDK.videoToggleEnabled = false;
            	AssistSDK.videoToggleButton.classList.remove("enabled");
            	AssistSDK.videoToggleButton.classList.add("disabled");
            }
            AssistSDK.enableOrDisableLocalVideoAndAudio();
        } else {
            AssistSDK.muted = false;
            AssistSDK.enableOrDisableLocalVideoAndAudio();
            //AssistSDK.muteButton.textContent = i18n.t("assistI18n:button.mute");
            AssistSDK.muteButton.classList.remove("muted");
            AssistSDK.muteButton.classList.add("unmuted");
            if (AssistSDK.videoToggleButton != null) {
            	AssistSDK.videoToggleEnabled = true;
            	AssistSDK.videoToggleButton.classList.remove("disabled");
            	AssistSDK.videoToggleButton.classList.add("enabled");
            }
        }
    },
    
    toggleVideo : function() {
    	if (!AssistSDK.videoToggleEnabled)
    		return;
    	if (AssistSDK.localVideoEnabled == false) {
    		AssistSDK.localVideoEnabled = true;
    		AssistSDK.videoToggleButton.classList.remove("muted");
    		AssistSDK.videoToggleButton.classList.add("unmuted");
    		AssistSDK.enableOrDisableLocalVideoAndAudio();
    	} else {
    		AssistSDK.localVideoEnabled = false;
    		AssistSDK.videoToggleButton.classList.remove("unmuted");
    		AssistSDK.videoToggleButton.classList.add("muted");
    		AssistSDK.enableOrDisableLocalVideoAndAudio();
    	}
    },
    
    enableOrDisableLocalVideoAndAudio : function() {
    	var enablingVideo = AssistSDK.localVideoEnabled && !AssistSDK.muted;
    	var enablingAudio = !AssistSDK.muted;
    	console.log("Setting video to " + ((enablingVideo) ? "enabled" : "disabled") + ".");
    	console.log("Setting audio to " + ((enablingAudio) ? "enabled" : "disabled") + ".");
    	if (AssistSDK.currentCall != null) {
    		AssistSDK.currentCall.setLocalMediaEnabled(enablingVideo, enablingAudio);
    	}
    },
    
    handleWindowResize : function() {
    	//Make sure agent window is within bounds.
    	for (var i = 0; i < AssistSDK.screenShareWindow.children.length; i++) {
    		var win = AssistSDK.screenShareWindow.children[i];
    		if (win.metadata.moveable) {
    			win.adjustPosition(0, 0);
    		}
    	}
    },

    handleUnload : function() {
        AssistSDK.log("In handleUnload()");
        AssistSDK.clearAnnotations();
        AssistSDK.isOnRecognizedAssistPage = false;
        setTimeout(AssistSDK.checkPage, 4000);
    },
    
    checkPage : function() {
        try {
            if (AssistSDK.sourceWindow.document.readyState !== "complete") {
                // Page not ready, but we're allowed to know that, which is promising. Try again later
                setTimeout(AssistSDK.checkPage, 500);
                return;
            }
        } catch (error) {
            // An error occurred trying to read the documents ready state, suggesting that we're not
            // on an assist enabled page.
        }
    	if (!AssistSDK.isOnRecognizedAssistPage) {
    		AssistSDK.isOnAssistPages = false;
    		AssistSDK.sendOffAssistPagesMessage();
        	alert(i18n.t("assistI18n:notice.navigateAwayAssist"));
        }
    },
    
    sendOffAssistPagesMessage : function() {
        var offAssistMessage = new Int16Array(1);
        offAssistMessage[0] = OFF_ASSIST_MESSAGE;
        AssistSDK.screenShareTopic.sendMessage(offAssistMessage);
    },
    
    sendOnAssistPagesMessage : function() {
    	var onAssistMessage = new Int16Array(1);
    	onAssistMessage[0] = ON_ASSIST_MESSAGE;
    	AssistSDK.screenShareTopic.sendMessage(onAssistMessage);
    },

    setCallQuality : function (cqIndicator, cqClass) {
        for (var i = 0; i < AssistSDK.cqClasses.length; i++) {
            if (cqClass != AssistSDK.cqClasses[i]) {
                cqIndicator.classList.remove(AssistSDK.cqClasses[i])
            } else {
                cqIndicator.classList.add(AssistSDK.cqClasses[i]);
            }
        }
    },
    
    connectWithAudio : function(configuration) {

        var usingAgentVideo = configuration.hasVideo();
        AssistSDK.usingAudio = true;
        var localVideoEnabled = configuration.hasLocalVideo();
        AssistSDK.localVideoEnabled = localVideoEnabled;
        AssistSDK.videoToggleEnabled = true;
        
        console.log("usingAgentVideo = " + usingAgentVideo + "; localVideoEnabled = " + localVideoEnabled);

        // create the UI in the actual window being supported
        AssistSDK.createRemoteUI((usingAgentVideo == false)); // createRemoteUI expects true if NOT using video ("skipVideo")
               
        AssistCSDK.init(configuration, function(newCall, correlationId) {
            AssistSDK.currentCall = newCall;
        
            configuration.setCorrelationId(correlationId);
            
            newCall.onRemoteMediaStream = function(remoteMediaStream) {
                console.log("on remote media stream");
                AssistSDK.remoteVideo = remoteMediaStream;
            };
            
            newCall.setVideoElement(AssistSDK.video);

            
            newCall.onInCall = function() {
                console.log("In onInCall()");

            	if (!AssistSDK.localVideoEnabled) {
                	newCall.setLocalMediaEnabled(false, true);
            	}
            	
            	if (usingAgentVideo) {
            		AssistSDK.video.style.visibility = "visible";
            	}
                AssistSDK.statusDiv.style.visibility = "hidden";
                statusDiv.textContent = i18n.t("assistI18n:popup.status.connected");
                
                AssistSDK.connectWebSocket(configuration);
                AssistSDK.doSDKCallback("onConnectionEstablished");
            };

            newCall.onEnded = function() {
            	console.log("In newCall.onEnded().");
            	AssistSDK.currentCall = null;
                AssistSDK.endSupport();
                AssistCSDK.destroySession();
            };

            newCall.onCallFailed = function(message) {
                AssistSDK.endSupport();
                AssistSDK.sourceWindow.console.log("Called failed: " + message);
           	 	AssistSDK.displayModal(i18n.t("assistI18n:notice.callFailed"));
            };

            newCall.onConnectionQualityChanged = function(quality) {
                AssistSDK.connectionQuality = quality;
                
                var cqIndicator = AssistSDK.sourceWindow.document.getElementById(
                    "call-quality-indicator");
                if (cqIndicator == null) {
                    cqIndicator = AssistSDK.sourceWindow.document.createElement("div");
                    cqIndicator.id = "call-quality-indicator";
 
                    AssistSDK.supportDiv.appendChild(cqIndicator);
                }
                var qualityClass;
                if (quality >= 90) {
                    qualityClass = AssistSDK.cqClasses[1];
                } else if (quality >= 70) {
                    qualityClass = AssistSDK.cqClasses[2];
                } else {
                    qualityClass = AssistSDK.cqClasses[3];
                }
                AssistSDK.setCallQuality(cqIndicator, qualityClass);
            };

            newCall.onBusy = newCall.onNotFound = newCall.onTimeout = function() {
                console.log("call was busy or not found");
                AssistSDK.displaySupportBusyModal();
                AssistSDK.endSupport();
            };
        });

        // return focus to the original window
        AssistSDK.sourceWindow.focus();
    },
       
    connectWithoutAudio : function(configuration) {
        AssistSDK.usingAudio = false;
        AssistSDK.localVideoEnabled = false;
        AssistSDK.videoToggleEnabled = false;
        
        // create the UI in the actual window being supported
        AssistSDK.createRemoteUI(true);
        AssistSDK.connectWebSocket(configuration);
        statusDiv.innerHTML = i18n.t("assistI18n:popup.status.connected");
        
        AssistSDK.doSDKCallback("onConnectionEstablished");

        // return focus to the original window
        AssistSDK.sourceWindow.focus();
    },

    doSDKCallback : function(callbackName, defaultImpl) {
    	try {
    		if (AssistSDK.sourceWindow.AssistSDK[callbackName]) {
    			return AssistSDK.sourceWindow.AssistSDK[callbackName]();
    		} else {
    			if (defaultImpl) {
    				return defaultImpl();
    			}
    		}
    	} catch (e) {
    		var errorMessage = "Error thrown in callback " + callbackName + ": " + e;
    		try {
    			AssistSDK.sourceWindow.console.warn(errorMessage);
    		} catch (e) {
    			console.warn(errorMessage);
    			Console.warn("(Couldn't log error to source window.)");
    		}
    		return undefined;
    	}
    },
    
    promptToAllowScreenShare: function () {
        var screenshareAllowed = AssistSDK.doSDKCallback("onScreenshareRequest", function() {
        	return window.confirm(i18n.t("assistI18n:notice.promptToAllowScreenShare"));
        });
        if (screenshareAllowed === undefined)
        	screenshareAllowed = false;
        return screenshareAllowed;
    },

    connectWebSocket : function(configuration) {
        AssistSDK.log("In connectWebSocket()");
        
        AssistAED.setConfig({ "url" : configuration.getUrl() });
        AssistAED.connectRootTopic(configuration.getCorrelationId(), function(rootTopic) {
            // Add the screen share subtopic
            AssistSDK.rootTopic = rootTopic;

            if (AssistSDK.screenShareAllowed) {
                AssistAED.startScreenShare();
            }

            rootTopic.participantJoined = function(newParticipant) {
                if (newParticipant.metadata.role == "agent") {
                    AssistSDK.setAgentName(newParticipant.metadata.name);
                    AssistSDK.setAgentPicture(newParticipant.metadata.avatar);
                    AssistSDK.setAgentText(newParticipant.metadata.text);
                }
            };
            // Listen for pushed urls
            rootTopic.messageReceived = function(source, messageBytes) {
                var messageString = String.fromCharCode.apply(null, messageBytes);
                var message = JSON.parse(messageString);
                switch (message.type) {
                    case "url":
                    AssistSDK.sourceWindow.location.href = message.url;
                        break;
                    case "requestScreenShare":
                        if ((!AssistSDK.screenShareTopic || AssistSDK.screenShareTopic.local) && AssistSDK.promptToAllowScreenShare()) {
                        	AssistSDK.allowScreenShare();
                        }
                        break;
                }
            };
            
        }, configuration.getSessionToken());
   },

    startScreenShare : function() {
        function setImageQualityScaleFactor(hostSharedWindow) {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function() {
                if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                    try {
                        var jsonConfig = JSON.parse(xmlHttp.responseText);
                        var quality = parseFloat(jsonConfig.scaleFactor);

                        if (!isNaN(quality)) {
                            hostSharedWindow.setImageQualityScaleFactor(quality);
                        }
                    } catch(e) {
                        console.log("error parsing json for image quality config");
                    }
                }
            };
            xmlHttp.open("GET", window.AssistConfig.getUrl() + "/assistserver/rest/config/consumer", true);
            xmlHttp.send();
        };

        function unmoveChildren(parent){
          while( parent.children.length > 0) {
            var sub = parent.children.pop();
            unmoveChildren(sub);
            sub.removeElementEventListeners();
            if (sub.metadata.moveable) {
              sub.element.classList.remove("moveable");
              $(sub.element).find('div.move-handle').remove();
              sub.moveHandle = undefined;
            }
           }
        }

        AssistSDK.rootTopic.openSubtopic({"type":"shared-window", "interactive":"true", "scrollable":"true"}, function(newTopic) {
            var glassPaneContainerDiv = AssistSDK.glassPane;

            if (AssistSDK.screenShareWindow) {            
              unmoveChildren(AssistSDK.screenShareWindow);
              AssistSDK.screenShareWindow.removeElementEventListeners();
              AssistSDK.screenShareWindow = null;
              AssistSDK.screenShareTopic = null;
              AssistSDK.videoWindow = null;
                }
                
            AssistSDK.screenShareWindow = new HostSharedWindow(newTopic, glassPaneContainerDiv);
            setImageQualityScaleFactor(AssistSDK.screenShareWindow);
            AssistSDK.screenShareTopic = newTopic;

            newTopic.participantJoined = function(newScreenParticipant) {
                // A new participant joined, send a resize event with the current size and a full screen refresh
                // force the sending of a resized event and a full refresh
                clearTimeout(AssistSDK.universalTimeout);
                AssistSDK.log("do rerender (new participant)");
                AssistSDK.universalTimeout = setTimeout(function() { AssistSDK.updateScreen(true) }, 500);
                AssistSDK.screenShareWindow.sendSizeAndPosition();
                AssistSDK.sendOnAssistPagesMessage();
            };
            
            if (AssistSDK.sourceWindow.getComputedStyle(AssistSDK.supportDiv).display !== "none") {
                AssistSDK.screenShareWindow.shareSubWindow(AssistSDK.supportDiv, {moveable: true, name:"draggable-agent-window", mustRemainVisiblePixels: 90, mustRemainVisibleBottomPixels: 120
                    //                    , resizeable: true, maintainAspect: true
                }, function(newWindow, newSubtopic) {
                    AssistSDK.videoWindow = newWindow;
                    newSubtopic.participantJoined = function(newVideoParticipant) {
                        setTimeout(function () {
                            AssistSDK.updateVideoWindow(true);
                            AssistSDK.videoWindow.sendSizeAndPosition();
                        }, 500);
                    };
                });
                AssistSDK.setupVideoWindowObserver();
            }

            // Look for and transmit form data
            AssistSDK.mapInputElements(AssistSDK.sourceWindow.document.body);
        });
        AssistSDK.rootTopic.subtopicClosed = function(closedTopic) {
        	if (closedTopic.metadata.type == "shared-window") {
        		console.log("shared-window topic closed.");
        	}
        };
    },

    sendEndSupportMessage : function() {
        AssistSDK.rootTopic.leave();
	}
	
};
